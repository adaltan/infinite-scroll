class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :message, :null => false
      t.timestamps
    end
  end
end
