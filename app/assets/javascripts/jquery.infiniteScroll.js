
// TODO: events? callbacks?



(function($){
    var K = {
        CUR_PAGE: 'infinite-scroll-cur-page',
        SETTINGS: 'infinite-scroll-settings'
    };

    var methods = {
        init: function(options){
            var $this = $(this);
            var settings = $.extend({
                'prependTo': '',
                'appendTo': '',
                'moreButton': '',
                'loadingIndicator': '',
                'url': '',
                'params': {},
                'startingPage': 0,
                'pageParamKey': 'page',
                'beforeQuery': null,
                'scrollWatch': true,
                'autoInitialLoading': true
            }, options);
            return this.each(function(){
                //
                $this.data(K.SETTINGS, settings);
                // "Load More" button
                $(settings.moreButton).show().click(function(){
                    $this.infiniteScroll('load_more');
                });
                // "Loading..." indicator
                $(settings.loadingIndicator).hide();
                // Current page
                $this.data(K.CUR_PAGE, settings.startingPage);
                // scrolling-handling
                if(settings.scrollWatch){
                    $(window).scroll(function(){
                        var c = $(document).height() - $(window).height();
                        if($(window).scrollTop() == c){
                            $this.infiniteScroll('load_more');
                        }
                    });
                }
                // Initial loading
                if(settings.autoInitialLoading){
                    $this.infiniteScroll('load_more');
                }
            });
        },

        load_more: function(){
            var $this = $(this);
            $this.infiniteScroll('indicate_now_loading');
            var cur_page = Number($this.data(K.CUR_PAGE));
            var settings = $this.infiniteScroll('get_settings');
            // "beforeQuery" callback
            var qry = settings.params;
            if(settings.beforeQuery){
                qry = settings.beforeQuery($this);
            }
            //
            qry[settings.pageParamKey] = ++ cur_page;
            //
            $.ajax({
                url: settings.url,
                dataType: 'text',
                data: qry,
                success: function(data){
                    $this.infiniteScroll('unindicate_now_loading');
                    $this.data(K.CUR_PAGE, cur_page);
                    $(data).appendTo(settings.appendTo);
                    $(data).prependTo(settings.prependTo);
                },
                error: function(xhr, status, err){
                    $this.infiniteScroll('unindicate_now_loading');                    
                    // TODO: fire error event
                }
            });
        },

        indicate_now_loading: function(){
            var $this = $(this);
            var settings = $this.infiniteScroll('get_settings');
            $(settings.loadingIndicator).fadeIn();
        },

        unindicate_now_loading: function(){
            var $this = $(this);
            var settings = $this.infiniteScroll('get_settings');
            $(settings.loadingIndicator).fadeOut();
        },

        get_settings: function(){
            var $this = $(this);
            var settings = $this.data(K.SETTINGS);
            return settings;
        }
    };

    $.fn.infiniteScroll = function(method) {
        if(methods[method]){
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        }else if(typeof method === 'object' || !method){
            return methods.init.apply(this, arguments);
        }else{
            $.error('Method ' +  method + ' does not exist on jQuery.infiniteScroll');
        }    
    };
})(jQuery);


