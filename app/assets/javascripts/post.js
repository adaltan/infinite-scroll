
// new postings

var toggle_save_post_button = function(b){
    var $button = $('#save_new_post_button');
    if(b){
        $button.removeAttr('disabled');        
    }else{
        $button.attr('disabled', 'disabled');
    }
};

$(document).ready(function(){
    var $button = $('#save_new_post_button');
    var $msg_textfield = $('#new_post_message');

    toggle_save_post_button(false);
    
    $msg_textfield.bind('keydown', function(){    
        toggle_save_post_button($msg_textfield.val().length>5);
    });

    $('#save_new_post_button').click(function(){
        $.post('/post/save', 
               {
                   message: $msg_textfield.val()
               }, 
               function(data){
                   $msg_textfield.val('');
                   toggle_save_post_button(false);
                   load_new_postings();
               }, 'text');
    });
});





var load_new_postings = function(){
    $('ul#post_entries_news').infiniteScroll('load_more');
};




// load postings

$(document).ready(function(){
    $('ul#post_entries').infiniteScroll({
        'moreButton': '#post_load_more',
        'loadingIndicator': '#posts_loader',
        'url': '/post/list',
        'appendTo': 'ul#post_entries'
    });

    $('ul#post_entries_news').infiniteScroll({
        'scrollWatch': false,
        'autoInitialLoading': false,
        'loadingIndicator': '#posts_loader',
        'url': '/post/list_incremental',
        'beforeQuery': function($this){
            var settings = $this.infiniteScroll('get_settings');
            var params = settings.params;
            var latest_post_id = function(){
                return _.max(
                    $("li.post_entry").map(function(i, elem){
                        return $(elem).data('id');
                    })
                );
            };
            params['last_id'] = latest_post_id();
            return params;
        },
        'prependTo': 'ul#post_entries'
    });

});



