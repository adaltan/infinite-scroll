class PostController < ApplicationController


  def index
  end


  def save
    p = Post.new
    p.message = params[:message]
    p.save
    render :text => 'Post Saved!'
  end



  def list
    @posts = Post
      .paginate(:page => params[:page] || 1, :per_page => 10)
      .order("created_at DESC")
    render :layout => nil
  end


  def list_incremental
    @posts = Post
      .where("id > :last_id", params)
      .order("created_at DESC")
    render :layout => nil, :template => 'post/list'
  end


  


end
