    -*- mode; markdown; coding: utf-8; -*-

# jQuery Infinite-Scroll Plugin

  * Author: [Jonghyouk Yun](mailto:ageldama@gmail.com)
  * License: [MIT License](http://en.wikipedia.org/wiki/MIT_License)
  


## Where to begin?

  1. see `app/assets/javascripts/jquery.infiniteScroll.js`
  1. this repository is self-contained Rails example application...
     1. `> bundle update`
     1. `> rake db:migrate`
     1. `> rails s`
     1. browse `http://localhost:3000` (port-number is vary...)


## Example
    
  Javascript:    
  
    // NOTE: add loaded entries to "<ul id='post_entries'>"...
    // NOTE: load from 
    $(document).ready(function(){
      $('ul#post_entries').infiniteScroll({
        'moreButton': '#post_load_more',
        'loadingIndicator': '#posts_loader',
        'url': '/post/list',
        'appendTo': 'ul#post_entries'
      });
    });
    
  
  
  Server-side:
  
    class PostController < ApplicationController
      #...
      def list
        @posts = Post.paginate(:page => params[:page] || 1, :per_page => 10)
        render :layout => nil
      end



## API


### Options

  * `prependTo` : jQuery selector string, element to `$.fn.prependTo()` loaded HTMLs. (Optional)
  * `appendTo` : jQuery selector string, element to `$.fn.appendTo()` loaded HTMLs. (Optional)
  * `moreButton` : "LOAD-MORE" action-triggering UI element jQuery selector string. Which will trigger `$.fn.infiniteScroll('load_more')`  (Optional)
  * `loadingIndicator` : loading indication UI element's jQuery selector string. (Optional)
  * `url` : string. (Required)
  * `params` : Map. Additional parameters to load data. (Optional)
  * `startingPage` : Number. (Optional, Default: 0)
  * `pageParamKey` : String, page-number indication parameter-key for load-data. (Optional, Default: "page")
  * `beforeQuery` : Callback, called before query to server. `fn($this) => new_params`. (Optional)
  * `scrollWatch` : Boolean, Specify scrolling trigger "load_more". (Optional, Default: true)
  * `autoInitialLoading` : Boolean, Specify load initial contents. (Optional, Default: true)


### Methods

  * `$(selector).infiniteScroll({...})` : Initiailizer. Pass "options".
  * `$(selector).infiniteScroll('load_more')` : CAN I HAZ MOA?
  * `$(selector).infiniteScroll('get_settings')` : Settings Object.



## TODOs

  * add callbacks (URL, Params...)
  * add events (pre/post-load...)
  * Whoops, VERSIONs?
